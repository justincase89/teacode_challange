This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

To run this project you need to install dependencies. Run this command in app folder:

**npm i** 

Next, in the same folder type:

**npm start**


Enjoy!




