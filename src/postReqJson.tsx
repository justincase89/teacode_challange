import React from 'react';
 
class JSONRequestComp extends React.Component<{}, any> {
    constructor(props: any) {
      super(props);
      this.state = {
        items: []
      };
    }
  
    componentDidMount() {
      fetch("https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com/users.json")
        .then(res => res.json())
        .then(
          (result) => {
            this.setState({
              items: result.items
            });
          }
        )
    }
  
    render() {
      const { items } = this.state;
        return (
          <ul>
            {items.map(item => (
              <li key={item.name}>
                {item.name}
              </li>
            ))}
          </ul>
        );
      }
    }

  export { JSONRequestComp };