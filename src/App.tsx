import React from 'react';
import './App.css';

import { JSONRequestComp } from './postReqJson';

function App() {
  return (
    <div className="container"> 

      <nav className="navbar navbar-dark bg-dark">
        <div className="navbar-name">Contacts</div>
      </nav>
    
      <br/>

      <form className="form-inline">
        <input className="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search"></input>
        <button className="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
      </form>

      <br/>

    <div className="table-responsive">
      <table id="tableContent" className="table">
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">First</th>
          <th scope="col">Last</th>
          <th scope="col">Check</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th scope="row"><img src="https://robohash.org/fugaperspiciatisea.png?size=25x25&set=set1" alt="avatar"/></th>
          <td>Alain</td>
          <td>Beedie</td>
          <td><input type="checkbox" className="form-check-input" id="exampleCheck1"></input></td>
        </tr>
        <tr>
          <th scope="row"><img src="https://robohash.org/reprehenderitquaenisi.png?size=25x25&set=set1" alt="avatar"/></th>
          <td>Mord</td>
          <td>Brownsall</td>
          <td><input type="checkbox" className="form-check-input" id="exampleCheck1"></input></td>
        </tr>
        <tr>
          <th scope="row"><img src="https://robohash.org/doloribusaspernaturea.png?size=25x25&set=set1" alt="avatar"/></th>
          <td>Finley</td>
          <td>Fenich</td>
          <td><input type="checkbox" className="form-check-input" id="exampleCheck1"></input></td>
        </tr>
        <tr>
          <th scope="row"><img src="https://robohash.org/sitdistinctiorerum.png?size=25x25&set=set1" alt="avatar"/></th>
          <td>Blane</td>
          <td>Fidge</td>
          <td><input type="checkbox" className="form-check-input" id="exampleCheck1"></input></td>
        </tr>
        <tr>
        <th scope="row"><img src="https://robohash.org/magniestporro.png?size=25x25&set=set1" alt="avatar"/></th>
          <td>Tiffany</td>
          <td>Glendinning</td>
          <td><input type="checkbox" className="form-check-input" id="exampleCheck1"></input></td>
        </tr>
        <tr>
          <th scope="row"><img src="https://robohash.org/officiisdoloremodit.png?size=25x25&set=set1" alt="avatar"/></th>
          <td>Sibyl</td>
          <td>Goodred</td>
          <td><input type="checkbox" className="form-check-input" id="exampleCheck1"></input></td>
        </tr>
        <tr>
          <th scope="row"><img src="https://robohash.org/repellendusinciduntamet.png?size=25x25&set=set1" alt="avatar"/></th>
          <td>Arney</td>
          <td>Hames</td>
          <td><input type="checkbox" className="form-check-input" id="exampleCheck1"></input></td>
        </tr>
        <tr>
          <th scope="row"><img src="https://robohash.org/fugiatautemodit.png?size=25x25&set=set1" alt="avatar"/></th>
          <td>Suzie</td>
          <td>Kydd</td>
          <td><input type="checkbox" className="form-check-input" id="exampleCheck1"></input></td>
        </tr>
        <tr>
          <th scope="row"><img src="https://robohash.org/etenimut.png?size=25x25&set=set1" alt="avatar"/></th>
          <td>Leicester</td>
          <td>Pellington</td>
          <td><input type="checkbox" className="form-check-input" id="exampleCheck1"></input></td>
        </tr>
        <tr>
          <th scope="row"><img src="https://robohash.org/autdeseruntexcepturi.png?size=25x25&set=set1" alt="avatar"/></th>
          <td>Ruperta</td>
          <td>Tollworth</td>
          <td><input type="checkbox" className="form-check-input" id="exampleCheck1"></input></td>
        </tr>
      </tbody>
    </table>
    </div>

    <button type="button" id="checker" className="btn btn-outline-success">Check contacts</button>

      <JSONRequestComp />

    </div>

  );
}


export default App;
